# Test run remote debug


Working test run for RemoteDebug library. Turns out the esp platform with version 3.x is not compatible with RemoteDebug library, so this test run project puts version number below 3 (<=2.7.4) to make this work.

See the ```platformio.ini``` file for clear picture of whats going on.

## Install

- Install vscode
- Search for platformio extension inside vscode extensions (```Ctrl + Shift + X```) and install it
- Open terminal and paste the command to clone this repository. ```git clone https://gitlab.com/makermindlane/test-run-remote-debug.git```
- Open this newly created directory (```test-run-remote-debug```) in vscode
- Build the project by pressing ```Ctrl + Alt + b```
